# Proximity and Temperature sensor

# Table of Contents
* Introduction
* Hardware Requirements
* Software Requirements
* Hardware Set Up
* Software Configuration
* Documentation Links
* Schematics and PCB
* License and Copyright

# Introduction
This is a system that monitors the temperature of anything within a certain proximity of the Microcontroller. This is a joint collaboration project and will contain all details regarding the configuration and usage of this Microcontroller HAT.

# Hardware Requirements
* 2 USB A to USB B cable
* 1 STM32F051 Discovery Board
* 1 Proxy Temp HAT
* 1 18650 Battery

# Software Requirements
* Atollic TrueSTUDIO
* Configuration Code

# Hardware Set Up
* Place the STM32F051 Discovery Board on top of the Proxy Temp HAT, placing the pins at the bottom of the STM32F051 Discovery Board into the corresponding holes at the top of the Proxy Temp HAT.

* Place the 18650 Battery in the battery holder on the Proxy Temp HAT and turn the switch on.

* Using one of the USB cables, connect the STM32F051 Discovery Board to the USB input on your laptop.

* Use the second USB cable to connect the Proxy Temp HAT to the USB input on your laptop.

# Software Configuration
* Download and Install Atollic TrueSTUDIO using the following link
https://www.st.com/en/development-tools/truestudio.html#get-software

* Download and unzip the 'ConfigCode' zip file containing the code used to set up the HAT

* Run the Atollic software and open the code file from the zip title 'Configuration'. Run this code to set up the sensors on the HAT.

* Open and run the code file from the zip titled 'Start' and observe the temperature output being displayed.

# Documentation Links:
https://gitlab.com/MasNet610/Water_level-Pressure-sensor/-/tree/main/Docs/Datasheets

# Schematics and PCB
## Proxy Temp HAT PCB Schematic

![Schematic_1](/uploads/1bf9c9597946634ca79fdbb4deda287d/Schematic_1.jpg)
![Schematic_2](/uploads/0409de5d5efe51c9180dc37a21e0ff03/Schematic_2.jpg)
![Schematic_3](/uploads/f431a98a69258814940cbc0698cb9553/Schematic_3.jpg)
![Schematic_4](/uploads/be27bb0107e61fcb2449fc3673a78458/Schematic_4.jpg)

## Proxy Temp HAT PCB Layout

![20220521_155024](/uploads/42460a22d646552d3614b2a4a546489f/20220521_155024.jpg)
![IMG_0027](/uploads/ebaa1e4925639e9dff71676c3337adcc/IMG_0027.JPG)

# License and Copyright
© Masase Netshituka, University of Cape Town.
Licensed under the [MIT License](LICENSE)
